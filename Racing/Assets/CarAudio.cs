using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarAudio : MonoBehaviour
{
    [SerializeField] private CarEngine _motor;
    [Header("Engine Sounds")]
    public AudioSource RunningSound;
    public float RunningMaxVolume;
    public float RunningMaxPitch;
    
    [Space]
    public AudioSource IdleSound;
    public float IdleMaxVolume;
    private float _speedRatio = 0;

    [Space]
    public float RevLimiterSound = 1f;
    public float RevLimiterFrequency = 3f;
    public float RevLimiterEngage = 0.8f;
    private float _revLimiter;

    private void FixedUpdate()
    {
        _speedRatio = _motor.GetSpeedRatio();
        if (_speedRatio > RevLimiterEngage)
        {
            _revLimiter = (Mathf.Sin(Time.time * RevLimiterFrequency) + 1f) * RevLimiterSound * (_speedRatio - RevLimiterEngage);
        }
        IdleSound.volume = Mathf.Lerp(0.1f, IdleMaxVolume, _speedRatio);
        RunningSound.volume = Mathf.Lerp(0.3f, RunningMaxVolume, _speedRatio);
        RunningSound.pitch = Mathf.Lerp(RunningSound.pitch, Mathf.Lerp(0.3f, RunningMaxPitch, _speedRatio) + _revLimiter, Time.fixedDeltaTime);
    }
}
