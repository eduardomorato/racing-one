using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    [SerializeField] private CarWheels _wheels;
    [SerializeField] private CarEngine _motor;

    private Rigidbody _rb;

    private float _slipAngle;
    public float SlipAngle { get => _slipAngle; }

    float throttleInput, steeringInput, brakeInput;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    private void GetInputs()
    {
        throttleInput = Input.GetAxis("Vertical");
        steeringInput = Input.GetAxis("Horizontal");
        brakeInput = Input.GetKey(KeyCode.Space) ? 1 : 0;
    }

    private void FixedUpdate()
    {
        GetInputs();

        _slipAngle = Vector3.Angle(transform.forward, _rb.velocity);
        if (_slipAngle < 120f)
        {
            if (throttleInput < 0)
            {
                brakeInput = Mathf.Abs(throttleInput);
                throttleInput = 0;
            }
            else
            {
                brakeInput = 0;
            }
        }

        float wheelAngle = Mathf.Clamp(Vector3.SignedAngle(transform.forward, _rb.velocity, Vector3.up), -_wheels.MaxTurnAngle, _wheels.MaxTurnAngle);

        _motor.HandleAcceleration(throttleInput);
        _wheels.HandeTurnAngle(steeringInput, throttleInput > 0 ? wheelAngle : 0);
        _motor.HandleBreakingForce(brakeInput);
    }
}
