using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CarEngine : MonoBehaviour
{
    [SerializeField] private int _maxSpeed = 200;
    [SerializeField] private float _maxTorque = 500f;
    [SerializeField] private float _maxBreakingPower = 50000f;

    private int _speed = 0;
    private float _torque = 500f;
    private float _breakingForce = 300f;
    private float _throttleInput = 0;
    private float _speedClamped = 0;

    public int Speed { get => _speed; }
    public float Torque { get => _torque; }

    public int MaxSpeed { get => _maxSpeed; }
    public float BreakingForce { get => _breakingForce; }
    public float MaxTorque { get => _maxTorque; }
    public float MaxBreakingPower { get => _maxBreakingPower; }

    [Space]
    [SerializeField] private Rigidbody _rb;
    [SerializeField] private CarAudio _carAudio;
    [SerializeField] private TMP_Text _speedText;

    private void FixedUpdate()
    {
        _speed = (int)(_rb.velocity.magnitude * 3.6f);
        _speedClamped = Mathf.Lerp(_speedClamped, _speed, Time.fixedDeltaTime);
        _speedText.text = _speed + " km/h";
    }

    public void HandleAcceleration(float throttle)
    {
        _throttleInput = throttle;
        if (Speed < MaxSpeed)
        {
            _torque = _maxTorque * throttle;
        }
        else
        {
            _torque = 0;
        }
    }

    public float GetSpeedRatio()
    {
        var gas = Mathf.Clamp(_throttleInput, 0.5f, 1f);
        return _speedClamped * gas / MaxSpeed;
    }

    public void HandleBreakingForce(float breakes)
    {
        _breakingForce = _maxBreakingPower * breakes;
    }
}
