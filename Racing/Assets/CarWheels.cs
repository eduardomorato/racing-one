using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TractionType { AWD, RWD, FWD }

public class CarWheels : MonoBehaviour
{
    public float MaxTurnAngle = 45f;
    public TractionType Traction = TractionType.RWD;
    public bool AutomaticDrift = false;
    private float _turnAngle = 0f;

    [Space]
    [SerializeField] private AnimationCurve _steeringAngleCurve;

    [Space]
    [SerializeField] private CarEngine _motor;
    [SerializeField] private CarController _controller;

    [Space]
    [SerializeField] private WheelCollider _wheelColliderFL;
    [SerializeField] private WheelCollider _wheelColliderFR;
    [SerializeField] private WheelCollider _wheelColliderBL;
    [SerializeField] private WheelCollider _wheelColliderBR;

    [SerializeField] private Transform _wheelTransformFL;
    [SerializeField] private Transform _wheelTransformFR;
    [SerializeField] private Transform _wheelTransformBL;
    [SerializeField] private Transform _wheelTransformBR;

    [Header("Wheel Effects")]
    [SerializeField] private GameObject _smokePrefab;
    [SerializeField] private ParticleSystem _wheelParticlesFL;
    [SerializeField] private ParticleSystem _wheelParticlesFR;
    [SerializeField] private ParticleSystem _wheelParticlesBL;
    [SerializeField] private ParticleSystem _wheelParticlesBR;
    private float _slipToSmoke = 0.3f;

    private void Start()
    {
        InstantiateSmoke();
    }

    public void HandeTurnAngle(float value, float angle)
    {
        _turnAngle = value * _steeringAngleCurve.Evaluate(_motor.Speed);
        _turnAngle += angle * (AutomaticDrift ? 1 : Mathf.Abs(value));
        _turnAngle = Mathf.Clamp(_turnAngle, -MaxTurnAngle, MaxTurnAngle);
    }

    private void FixedUpdate()
    {
        MoveWheels();
        ApplyBreakes();
        Steering();

        UpdateWheel(_wheelColliderFL, _wheelTransformFL);
        UpdateWheel(_wheelColliderFR, _wheelTransformFR);
        UpdateWheel(_wheelColliderBL, _wheelTransformBL);
        UpdateWheel(_wheelColliderBR, _wheelTransformBR);

        CheckParticles();
    }

    void Steering()
    {
        _wheelColliderFL.steerAngle = _turnAngle;
        _wheelColliderFR.steerAngle = _turnAngle;
    }

    void ApplyBreakes()
    {
        _wheelColliderFL.brakeTorque = _motor.BreakingForce * 0.7f;
        _wheelColliderFR.brakeTorque = _motor.BreakingForce * 0.7f;
        _wheelColliderBL.brakeTorque = _motor.BreakingForce * 0.3f;
        _wheelColliderBR.brakeTorque = _motor.BreakingForce * 0.3f;
    }

    void MoveWheels()
    {
        switch (Traction)
        {
            case TractionType.RWD:
                _wheelColliderBL.motorTorque = _motor.Torque;
                _wheelColliderBR.motorTorque = _motor.Torque;
                break;
            case TractionType.AWD:
                _wheelColliderFL.motorTorque = _motor.Torque;
                _wheelColliderFR.motorTorque = _motor.Torque;
                _wheelColliderBL.motorTorque = _motor.Torque;
                _wheelColliderBR.motorTorque = _motor.Torque;
                break;
            case TractionType.FWD:
                _wheelColliderFL.motorTorque = _motor.Torque;
                _wheelColliderFR.motorTorque = _motor.Torque;
                break;
        }
    }

    void UpdateWheel(WheelCollider col, Transform trans)
    {
        Vector3 position;
        Quaternion rotation;
        col.GetWorldPose(out position, out rotation);
        trans.position = position;
        trans.rotation = rotation;
    }

    void InstantiateSmoke()
    {
        _wheelParticlesFL = Instantiate(_smokePrefab, _wheelColliderFL.transform.position - Vector3.up * _wheelColliderFL.radius, Quaternion.identity, _wheelColliderFL.transform).GetComponent<ParticleSystem>();
        _wheelParticlesFR = Instantiate(_smokePrefab, _wheelColliderFR.transform.position - Vector3.up * _wheelColliderFR.radius, Quaternion.identity, _wheelColliderFR.transform).GetComponent<ParticleSystem>();
        _wheelParticlesBL = Instantiate(_smokePrefab, _wheelColliderBL.transform.position - Vector3.up * _wheelColliderBL.radius, Quaternion.identity, _wheelColliderBL.transform).GetComponent<ParticleSystem>();
        _wheelParticlesBR = Instantiate(_smokePrefab, _wheelColliderBR.transform.position - Vector3.up * _wheelColliderBR.radius, Quaternion.identity, _wheelColliderBR.transform).GetComponent<ParticleSystem>();
    }

    void CheckParticles()
    {
        WheelHit[] wheelHits = new WheelHit[4];
        _wheelColliderFL.GetGroundHit(out wheelHits[0]);
        _wheelColliderFR.GetGroundHit(out wheelHits[1]);
        _wheelColliderBL.GetGroundHit(out wheelHits[2]);
        _wheelColliderBR.GetGroundHit(out wheelHits[3]);

        if ((Mathf.Abs(wheelHits[0].sidewaysSlip) + Mathf.Abs(wheelHits[0].forwardSlip) > _slipToSmoke)) _wheelParticlesFL.Play();
        else _wheelParticlesFL.Stop();

        if ((Mathf.Abs(wheelHits[1].sidewaysSlip) + Mathf.Abs(wheelHits[1].forwardSlip) > _slipToSmoke)) _wheelParticlesFR.Play();
        else _wheelParticlesFR.Stop();

        if ((Mathf.Abs(wheelHits[2].sidewaysSlip) + Mathf.Abs(wheelHits[2].forwardSlip) > _slipToSmoke)) _wheelParticlesBL.Play();
        else _wheelParticlesBL.Stop();

        if ((Mathf.Abs(wheelHits[3].sidewaysSlip) + Mathf.Abs(wheelHits[3].forwardSlip) > _slipToSmoke)) _wheelParticlesBR.Play();
        else _wheelParticlesBR.Stop();
    }

}
